const quizData = [
    {
      question: 'Who is called the first citizen of India?',
      a: 'Mahatma Gandhi',
      b: 'Subhas Chandra Bose',
      c: 'Prime Minister',
      d: 'President' , 
      correct: 'd' 
    },
    {
        question: 'How many states does India have?',
        a: '28',
        b: '29',
        c: '30' ,
        d: '31',
        correct: 'a'   
    },
    {
        question: 'How many colors does Indian flag have?',
        a: '1',
        b: '4',
        c: '3' ,
        d: '2',
        correct: 'b'   
    },
    {
        question: 'Who wrote Bhagavadgita?',
        a: 'Saint Manu',
        b: 'Bheeshma',
        c: 'Vyasa' ,
        d: 'Rama',
        correct: 'c'   
    },
    
    {
        question: 'How many bones does a Human have?',
        a: '206',
        b: '218',
        c: '223',
        d: '298',
        correct: 'a'   
    },
    
    
];
const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");

let currentQuiz = 0;
let score = 0;

loadQuiz();

function loadQuiz() {
    deselectAnswers();

    const currentQuizData = quizData[currentQuiz];

    questionEl.innerText = currentQuizData.question;
    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;
}

function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });

    return answer;
}

function deselectAnswers() {
    answerEls.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn.addEventListener("click", () => {
    // check to see the answer
    const answer = getSelected();

    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }

        currentQuiz++;
        if (currentQuiz < quizData.length) {
            loadQuiz();
        } else {
            quiz.innerHTML = `
                <h2>You answered correctly at ${score}/${quizData.length} questions.</h2>
                
                <button onclick="location.reload()">Reload</button>
            `;
        }
    }
});
